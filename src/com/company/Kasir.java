package com.company;

import java.util.Scanner;

public class Kasir {

    private Meja[] daftarMeja;
    private Menu[] daftarMenu;

    public Kasir() {
        daftarMeja = new Meja[10];
        for (int i = 0; i < 10; i++) {
            daftarMeja[i] = new Meja(i + 1);
        }

        daftarMenu = new Menu[5];
        daftarMenu[0] = new Menu("Nasi Goreng", 15000);
        daftarMenu[1] = new Menu("Mi Goreng", 15000);
        daftarMenu[2] = new Menu("Capcay", 20000);
        daftarMenu[3] = new Menu("Bihun Goreng", 17000);
        daftarMenu[4] = new Menu("Ayam Koloke", 25000);
    }

    // digunakan untuk menampilkan daftar meja beserta keterangan ketersediaannya
    // gunakan method isKosong pada class Kasir agar lebih mudah
    public void tampilkanDaftarMeja() {
        //EDIT DISINI
        System.out.println("Daftar Meja:");
        //menggunakan loop utnuk mengecek setiap Meja dalam array daftarMeja.
        //untuk setiap meja, method akan menggunakan method isKosong() untuk cek meja kosong atau tidak.
        //jika meja kosong, maka nomor meja bakal masuk. jika meja sudah terisi, maka akan keluar output meja sudah terisi.
        for (Meja meja : daftarMeja) {
            if (meja.isKosong()) {
                System.out.println("Meja " + meja.getNomorMeja() + " (kosong)");
            } else {
                System.out.println("Meja " + meja.getNomorMeja() + " (terisi oleh " + meja.getPelanggan().getNama() + ")");
            }
        }
    }

    // untuk menambahkan pelanggan pada meja tertentu
    // jika meja kosong tambahkan pelanggan pada meja tersebut
    // jika tidak buatlah keterangan bahwa meja sudah ada pelanggan
    // method ini untuk memeriksa apakah meja yang dipilih masih kosong menggunakan method isKosong.
    // jika meja kosong, maka akan ditambahkan pelanggan ke meja tersebut. jika meja sudah terisi, maka akan keluar output meja sudah terisi.
    public void tambahPelanggan(int nomorMeja, Pelanggan pelanggan) { //parameter nomorMeja digunakan untuk menentukan meja mana yang akan digunakan untuk menambahkan pelanggan dan parameter pelanggan adalah objek Pelanggan yang akan ditambahkan.
        //EDIT DISINI
        if (daftarMeja[nomorMeja - 1].isKosong()) {
            daftarMeja[nomorMeja - 1].setPelanggan(pelanggan);
        } else {
            System.out.println("Meja sudah terisi");
        }
    }

    // menambah pesanan menu pada nomor meja
    // jika menu tidak ada dalam daftar maka tampilkan "Menu is null"
    // method akan memeriksa apakah meja kosong atau tidak dengan memanggil method isKosong() pada objek meja.
    public void tambahPesanan(int nomorMeja, Menu menu) {
        //EDIT DISINI
        Meja meja = daftarMeja[nomorMeja - 1];
        if (meja.isKosong()) {  // jika meja kosong, maka method akan mencetak output ini.
            System.out.println("Meja " + nomorMeja + " kosong, silahkan tambah pelanggan terlebih dahulu");
        } else {   // jika meja tidak kosong, maka method akan memeriksa apakah menu yang ingin dipesan tidak kosong.
            if (menu == null) {
                System.out.println("Menu is null");
            } else {  //jika menu tidak kosong, maka method akan memanggil method setMenu(menu) pada objek meja untuk menambahkan pesanan menu pada meja.
                meja.setMenu(menu);
            }
        }
    }

    // Menghapus pelanggan
    public void hapusPelanggan(int nomorMeja) {
        //EDIT DISINI
        Meja meja = daftarMeja[nomorMeja - 1];
        // mencari data pelanggan pada meja tersebut dengan memanggil method getPelanggan()
        Pelanggan pelanggan = meja.getPelanggan();
        if (pelanggan != null) {  // jika ditemukan data pelanggan pada meja tersebut, maka data pelanggan akan dihapus dari meja tersebut dengan memanggil method setPelanggan(null)
            meja.setPelanggan(null);
            System.out.println("Pelanggan pada Meja " + nomorMeja + " berhasil dihapus.");
        } else {  // jika tidak ditemukan data pelanggan pada meja tersebut, maka akan ditampilkan pesan "Maaf, tidak ada pelanggan pada Meja nomorMeja.
            System.out.println("Maaf, tidak ada pelanggan pada Meja " + nomorMeja + ".");
        }
    }

    public int hitungHargaPesanan(int nomorMeja) {
        int totalHarga = 0;
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    totalHarga += menu[i].getHarga();
                }
            }
            return totalHarga;
        }
        return totalHarga;
    }

    public void tampilkanPesanan(int nomorMeja) {
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    System.out.println("Meja " + nomorMeja + " - " + pelanggan.getNama() + " memesan " + menu[i].getNama() + " seharga " + menu[i].getHarga());
                }
            }
        } else {
            System.out.println("Meja " + nomorMeja + " tidak memiliki pesanan");
        }
    }

    public void tampilkanDaftarMenu() {
        System.out.println("Daftar Menu:");
        System.out.println("1. Nasi Goreng - Rp15.000");
        System.out.println("2. Mi Goreng - Rp15.000");
        System.out.println("3. Capcay - Rp20.000");
        System.out.println("4. Bihun Goreng - Rp17.000");
        System.out.println("5. Ayam Koloke - Rp25.000");
        System.out.println("6. Simpan");
        System.out.println();
    }

    public void tampilkanDaftarFitur() {
        System.out.println("1. Tampilkan daftar meja");
        System.out.println("2. Tambah pelanggan");
        System.out.println("3. Tambah pesanan");
        System.out.println("4. Hapus pelanggan");
        System.out.println("5. Hitung harga pesanan");
        System.out.println("6. Tampilkan pesanan di meja");
        System.out.println("0. Keluar");
    }

    public void jalankan() {
        Scanner scanner = new Scanner(System.in);
        int pilihan = -1;
        while (pilihan != 0) {
            tampilkanDaftarFitur();
            System.out.print("Masukkan pilihan: ");
            pilihan = scanner.nextInt();
            scanner.nextLine();
            switch (pilihan) {
                case 1:
                    // menampilkan daftar meja dengan method yang sudah ada
                    //EDIT DISINI
                    tampilkanDaftarMeja();  //memanggil method untuk menampilkan daftar meja.
                    break;
                case 2:
                    // tampilkan pesan untuk input nomor meja dan nama pelanggan untuk digunakan pada method
                    // jangan lupa instansiasi Pelanggan dengan nama pelanggan sesuai input
                    // EDIT DISINI
                    System.out.print("Masukkan nomor meja: ");
                    // untuk mengambil nomor meja dari input user kemudian disimpan pada variabel nomorMejaTambah.
                    int nomorMejaTambah = scanner.nextInt();
                    scanner.nextLine();
                    System.out.print("Masukkan nama pelanggan: ");
                    // untuk mengambil nama pelanggan dari input user dan disimpan pada variabel namaPelanggan.
                    String namaPelanggan = scanner.nextLine();
                    // method ini akan menambahkan pelanggan baru pada meja dengan nomor yang diberikan.
                    tambahPelanggan(nomorMejaTambah, new Pelanggan(namaPelanggan));
                    break;
                case 3:
                    boolean stopLoop = false;
                    System.out.print("Masukkan nomor meja: ");
                    int nomorMejaPesan = scanner.nextInt();
                    Boolean meja = daftarMeja[nomorMejaPesan - 1].isKosong();
                    scanner.nextLine();
                    if (!meja) {
                        tampilkanDaftarMenu();
                        while (!stopLoop) {
                            System.out.print("Masukkan nomor menu: ");
                            int nomorMenuPesan = scanner.nextInt();
                            scanner.nextLine();
                            switch (nomorMenuPesan) {
                                case 1:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[0]);
                                    break;
                                case 2:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[1]);
                                    break;
                                case 3:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[2]);
                                    break;
                                case 4:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[3]);
                                    break;
                                case 5:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[4]);
                                    break;
                                case 6:
                                    stopLoop = true;
                                    break;
                                default:
                                    System.out.println("Nomor menu tidak valid");
                                    break;
                            }
                        }
                    }
                    else {
                        System.out.println("Meja belum terisi oleh pelanggan");
                    }
                    break;
                case 4:
                    // untuk menghapus pelanggan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja yang akan dihapus untuk digunakan pada method hapusPelanggan()
                    // EDIT DISINI
                    System.out.print("Masukkan nomor meja: ");
                    // untuk mengambil nomor meja dari input user kemudian disimpan pada variabel nomorMejaHapus.
                    int nomorMejaHapus = scanner.nextInt();
                    scanner.nextLine();
                    // untuk memanggil metode hapusPelanggan
                    hapusPelanggan(nomorMejaHapus);
                    break;
                case 5:
                    // Untuk melihat total harga pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja
                    // jangan lupa membedakan keluaran apabila pelanggan belum memesan apapun / total harga 0
                    // EDIT DISINI
                    System.out.print("Masukkan nomor meja: ");
                    // untuk mengambil nomor meja dari input user kemudian disimpan pada variabel nomorMejaHitung.
                    int nomorMejaHitung = scanner.nextInt();
                    scanner.nextLine();
                    double totalHarga = hitungHargaPesanan(nomorMejaHitung);
                    if (totalHarga == 0) { // jika meja tersebut tidak memiliki pesanan, maka outputnya ini.
                        System.out.println("Meja " + nomorMejaHitung + " tidak memilki pesanan");
                    } else { // jika ada pesanan, outputnya ini
                        System.out.println("Harga pesanan di meja " + nomorMejaHitung + " adalah " + totalHarga);
                    }
                    break;
                case 6:
                    // untuk melihat pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja
                    // EDIT DISINI
                    System.out.print("Masukkan nomor meja: ");
                    // untuk mengambil nomor meja dari input user kemudian disimpan pada variabel nomorMejaTampil.
                    int nomorMejaTampil = scanner.nextInt();
                    scanner.nextLine();
                    // memanggil method untuk tampilkan pesanan dengan parameter nomorMejaTampil.
                    tampilkanPesanan(nomorMejaTampil);
                    break;

                case 0:
                    System.out.println("Terima kasih telah menggunakan aplikasi kasir restoran!");
                    break;
                default:
                    System.out.println("Pilihan tidak valid");
                    break;
            }
            System.out.println();
        }
        scanner.close();
    }
}
